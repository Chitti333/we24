Write functions that computes the attacking positions of some chess pieces

1.rook_pos(pos) - takes position of the rook and returns the list of possible position that rook can attack. -- assigned to chinmayie

1.bishop_pos(pos) - takes position of the bishop and returns the list of possible position that bishop can attack. -- assigned to chitti

3.knight_pos(pos) - takes position of the knight (string) and returns the list of possible position that knight can attack. -- assigned to kashika

4.queen_pos(pos)- takes position of the queen and returns the list of possible position that queen can attack. -- assigned to chitti

5.is\attacking(piece1,position1,piece2,position2) 
        check if the piece @ position1 can attack piece@position2 and vice-versa