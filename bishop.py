RANKS = 'ABCDEFGH'
FILES = '12345678'

def bishop(position):

    possibilities = []
    a,n = position[0], position[1]
    rank1, rank2 = RANKS.split(a)
    file1, file2 = FILES.split(n)
    right_top = [rt for rt in zip(rank2,file2)]
    left_bottom = [lb for lb in zip(rank1,file1)]
    left_top = [lt for lt in zip(reversed(rank1),file2)]
    right_bottom = [rb for rb in zip((rank2),reversed(file1))]
    return  right_top + left_bottom + right_bottom + left_top
    
print(bishop('C3'))