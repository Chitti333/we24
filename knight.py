def knight(position: str) -> list[str]:
    possibility = []
    ans = []
    letter = position[0]
    num = position[1]
    all_letter = "zabcdefghi"
    operator = [(1,2),(1,-2),(-1,2),(-1,-2),(2,1),(2,-1),(-2,1),(-2,-1)]
    for i,j in operator:
        possible = f"{all_letter[all_letter.index(letter) + i]}{int(num) + j}"
        possibility.append(possible)

    for i in possibility:
        if ('a' <= i[0] <= 'h') and (1 <= int(i[1:]) <= 8):
            ans.append(i)

    return ans